# Exception Module for the API-Server
This project holds the Exception module for the API-Server. It supports a unified exception handling using the JsonAPI Error format.

## Installation
To use the provided BaseHandler just make your existing Exception Handler in
`app/Exceptions/Handler.php` extend from BaseHandler:

```
use ApiServer\Exceptions\Exceptions\BaseHandler;

class Handler extends BaseHandler
{
```

## Depencies
 * No depencies

## Provides
 * Various Exceptions

## Documentation
 * [Technology stack](doc/technology.md)
 * [Unittesting](doc/unittesting.md)
 * [Creating a release](doc/release.md)

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

# License
See [License](LICENSE.txt)
